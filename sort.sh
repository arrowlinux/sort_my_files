#!/usr/bin/env bash
# vim: noai:ts=4:sw=4:expandtab
# shellcheck source=/dev/null
# shellcheck disable=2009


#------------------------------------------------------------------------------
# Created by ELIAS WALKER
#------------------------------------------------------------------------------
# started 28/Jan/2020 undergoing development to date.
#------------------------------------------------------------------------------

#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; under version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not,  visit
# https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
###############################################################################
####################### Acknowledgements ######################################
###############################################################################
#
# So far, to best of my knowledge. Just me. Elias Walker
###############################################################################
###############################################################################
#       If you find this script useful
#       help keep this project afloat. donate to
#       my Bitcoim wallet at 
#       bc1q0ccha04u5cvvrulvgnmh3u4wfl4wpfxeksqusw
#       
#       or
#       paypal.me/eliwal
#
#
###############################################################################
#

USER_HEIGHT=18
USER_WIDTH=42
USER_CHOICE_HEIGHT=15

ExT="Do nothing and EXIT"
BTTL="Sort-Me"
MENU="Choose a Directory:"
CHoP="Sort files the easy way."

clr(){

    clear ; clear

}
#################################################################
if [[ $USER = "root"  ]]
   then 
        ERR "NEVER run this as ROOT. Exiting now..."; exit 86
   else
      if [ -f /usr/bin/dialog ]
             then clr
                 echo " Dialog is already installed"
                 echo ""
             else sudo apt-get install -y dialog
                  clr
      fi
fi


PD1="$(pwd)/"
USR1="$(logname)" 
PD2="/home/$USER"
Pd3="/home/elias/GitLab/my-scripts"

 clr
dialog --title "SORT" --no-lines --msgbox "YOU ARE IN    $PD1 \n\nThis will move ALL the files from The Directory you chose \ninto their respective folders. \n\nsong.mp3 will be moved into /home/$USR1/Music/ \nmovie.mp4 will be moved into /home/$USR1/Movies/ \nletter.doc will be moved into /home/$USR1/Documents/ \nAnd so on, \n\nIf you chose the same Directory that this script is in\nThis script will be moved to     /home/$USER/GitLab/my-scripts/ \n\nWarning DO NOT run this as ROOT or in the ROOT DIRECTORY! \nThat CAN Trash your system! Otherwise this should be system safe. \n\nIn the next window you will chose the folder you want to clean.\n\nAre you ready to continue?" 25 70
clr

while [[ "$?" = "0" ]] ; do

your_directory_choice(){
 # while [[ "$?" = "0" ]] ; do
                 OPTIONS=(1 "$PD1"
                          2 "~/Public"
                          3 "~/Documents"
                          4 "~/my-scripts" 
                          5 "~/Music"
                          6 "~/Videos"
                          7 "~/Pictures"
                          8 "~/Desktop"
                          9 "~/Downloads"
                          10 "~/Compressed"
                          11 "EXIT Sort")
                 
                 CHOICE=$(dialog --clear \
                                 --default-item 11 \
                                 --ok-label "Continue" \
                                 --cancel-label "EXIT" \
                                 --no-lines \
                                 --backtitle "$BTTL" \
                                 --title "$CHoP" \
                                 --menu "$MENU" \
                                 $USER_HEIGHT $USER_WIDTH $USER_CHOICE_HEIGHT \
                                 "${OPTIONS[@]}" \
                                 2>&1 >/dev/tty)
                 case $CHOICE in



                         1)
                           dr1="$PD1"
                            ;;

                         2)
                           dr1=" $PD2/Public/"
                           ;;

                         3)
                           dr1=" $PD2/Documents/"
                           ;;

                         4)
                           dr1=" $PD2/my-scripts/"
                            ;;
                        
                         5)
                           dr1=" $PD2/Music/"
                           ;;
                        
                         6)
                           dr1=" ~/Videos/"
                           ;;
                        
                         7)
                           dr1=" $PD2/Pictures/"
                           ;;
                        
                         8) 
                           dr1=" $PD2/Desktop/"
                           ;;
                        
                         9)
                           dr1=" $PD2/Downloads/"
                           ;;
                        
                         10)
                            dr1" $PD2/Compressed/"
                            ;;
                        
                         11) 
                            clr
                            exit 0
                             ;;
               
                esac

                clr
      
}



Music()
{
# if [ -e "$PD1/Music" ];then
if [ -e "$PD2/Music" ];then
       echo -n ""
else
       mkdir $PD2/Music
fi
mv $dr1*.webm "$PD2/Music" 2>/dev/null
mv $dr1*.vox "$PD2/Music" 2>/dev/null
mv $dr1*.wma "$PD2/Music" 2>/dev/null
mv $dr1*.aac "$PD2/Music" 2>/dev/null
mv $dr1*.ogg "$PD2/Music" 2>/dev/null
mv $dr1*.m4a "$PD2/Music" 2>/dev/null
mv $dr1*.raw "$PD2/Music" 2>/dev/null
mv $dr1*.mp3 "$PD2/Music" 2>/dev/null
mv $dr1*.wav "$PD2/Music" 2>/dev/null
mv $dr1*.midi "$PD2/Music" 2>/dev/null
echo ""
}

Movies()
{
if [ -e "$PD2/Videos" ];then
       echo -n ""
else
       mkdir $PD2/Videos
fi
mv $dr1*.mkv "$PD2/Videos" 2>/dev/null
mv $dr1*.flv "$PD2/Videos" 2>/dev/null
mv $dr1*.qt "$PD2/Videos" 2>/dev/null
mv $dr1*.mov "$PD2/Videos" 2>/dev/null
mv $dr1*.mp4 "$PD2/Videos" 2>/dev/null
mv $dr1*.3gp "$PD2/Videos" 2>/dev/null
mv $dr1*.avi "$PD2/Videos" 2>/dev/null
mv $dr1*.mpeg "$PD2/Videos" 2>/dev/null
mv $dr1*.mpg "$PD2/Videos" 2>/dev/null
echo ""
}

Pictures()
{
if [ -e "$PD2/Pictures" ];then
       echo -n ""
else
       mkdir $PD2/Pictures
fi
mv $dr1*.xcf "$PD2/Pictures" 2>/dev/null
mv $dr1*.bmp "$PD2/Pictures" 2>/dev/null
mv $dr1*.tif "$PD2/Pictures" 2>/dev/null
mv $dr1*.jpeg "$PD2/Pictures" 2>/dev/null
mv $dr1*.jpg "$PD2/Pictures" 2>/dev/null
mv $dr1*.gif "$PD2/Pictures" 2>/dev/null
mv $dr1*.png "$PD2/Pictures" 2>/dev/null
mv $dr1*.PNG "$PD2/Pictures" 2>/dev/null
echo ""
}

Documents()
{
if [ -e "$PD2/Documents" ];then
       echo -n ""
else
       mkdir $PD2/Documents
fi
mv $dr1*.wpd "$PD2/Documents" 2>/dev/null
mv $dr1*.sxw "$PD2/Documents" 2>/dev/null
mv $dr1*.fb2 "$PD2/Documents" 2>/dev/null
mv $dr1*.htm "$PD2/Documents" 2>/dev/null
mv $dr1*.html "$PD2/Documents" 2>/dev/null
mv $dr1*.ps "$PD2/Documents" 2>/dev/null
mv $dr1*.rtf "$PD2/Documents" 2>/dev/null
mv $dr1*.doc "$PD2/Documents" 2>/dev/null
mv $dr1*.pdf "$PD2/Documents" 2>/dev/null
mv $dr1*.docx "$PD2/Documents" 2>/dev/null
mv $dr1*.bak "$PD2/Documents" 2>/dev/null
mv $dr1*.md "$PD2/Documents" 2>/dev/null
mv $dr1*.odt "$PD2/Documents" 2>/dev/null
mv $dr1*.txt "$PD2/Documents" 2>/dev/null
echo ""
}

my_scripts()
{
if [ -e "$PD2/GitLab/my-scripts" ];then
       echo -n ""
else
       mkdir $PD2/GitLab/my-scripts
fi
#######################################
### Uncomment what you want moved.  ###
### but USE CAUTION! Some of these  ###
###  are used buy your system.      ###
#######################################
mv $dr1*.sh "$PD2/GitLab/my-scripts" 2>/dev/null
mv $dr1*.py "$PD2/GitLab/my-scripts" 2>/dev/null
#mv $dr1*.html "$PD2/GitLab/my-scripts" 2>/dev/null
#mv $dr1*.xml "$PD2/GitLab/my-scripts" 2>/dev/null
#mv $dr1*.js "$PD2/GitLab/my-scripts" 2>/dev/null
#mv $dr1*.json "$PD2/GitLab/my-scripts" 2>/dev/null
#mv $dr1*.vbs "$PD2/GitLab/my-scripts" 2>/dev/null
#mv $dr1*.scpt "$PD2/GitLab/my-scripts" 2>/dev/null
#mv $dr1*.php "$PD2/GitLab/my-scripts" 2>/dev/null
#mv $dr1*.asp "$PD2/GitLab/my-scripts" 2>/dev/null
echo ""
}

Public()
{
if [ -e "$PD2/Public" ];then
       echo -n ""
else
       mkdir $PD2/Public
fi
mv $dr1*.iso "$PD2/Public" 2>/dev/null
mv $dr1*.exe "$PD2/Public" 2>/dev/null
echo ""
}

Compressed()
{
if [ -e "$PD2/Compressed" ];then
       echo -n ""
else
       mkdir $PD2/Compressed
fi
mv $dr1*.bat "$PD2/Compressed" 2>/dev/null
mv $dr1*.deb "$PD2/Compressed" 2>/dev/null
mv $dr1*.zip "$PD2/Compressed" 2>/dev/null
mv $dr1*.rar "$PD2/Compressed" 2>/dev/null
mv $dr1*.7z "$PD2/Compressed" 2>/dev/null
mv $dr1*.tar "$PD2/Compressed" 2>/dev/null
mv $dr1*.tar.bz2 "$PD2/Compressed" 2>/dev/null
mv $dr1*.tar.gz "$PD2/Compressed" 2>/dev/null
echo ""
}

################################################
################################################
# Order Of Execution
your_directory_choice
Music
Movies
Pictures
Documents
my_scripts
Public
Compressed


# this script only moves file formats listed

dialog --title "SORT" --no-lines --yesno "SORT has moved ALL files \ninto their respective folders. \nWhould you like to Sort another folder?" 8 45
   if [[ "$?" = "1" ]]
      then
          clr
          exit 86
   fi
clr
done
